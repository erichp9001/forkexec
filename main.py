import sys
py = sys.executable
import os
print py

if os.fork():
    os.close(0)
    os.open("/dev/null",os.O_RDONLY)
    os.read(0,1)
    prog = [py, "child.py"]
    os.execvp(py,prog)

else:
    print "parent"